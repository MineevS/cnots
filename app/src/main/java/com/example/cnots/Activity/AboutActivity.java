package com.example.cnots.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.cnots.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Включаем отображение кнопки назад на панели ActionBar.
    }

    @Override
    public boolean onSupportNavigateUp() // Обработчик кнопки возврата.
    {
        finish();
        return super.onSupportNavigateUp();
    }
}