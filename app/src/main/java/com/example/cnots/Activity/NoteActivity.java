package com.example.cnots.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.example.cnots.R;
import com.example.cnots.SQLite.DBHelper;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.maps.zzaa;
import com.google.android.gms.internal.maps.zzad;
import com.google.android.gms.internal.maps.zzag;
import com.google.android.gms.internal.maps.zzl;
import com.google.android.gms.internal.maps.zzo;
import com.google.android.gms.internal.maps.zzr;
import com.google.android.gms.internal.maps.zzx;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.internal.IGoogleMapDelegate;
import com.google.android.gms.maps.internal.ILocationSourceDelegate;
import com.google.android.gms.maps.internal.IProjectionDelegate;
import com.google.android.gms.maps.internal.IUiSettingsDelegate;
import com.google.android.gms.maps.internal.zzab;
import com.google.android.gms.maps.internal.zzaf;
import com.google.android.gms.maps.internal.zzah;
import com.google.android.gms.maps.internal.zzal;
import com.google.android.gms.maps.internal.zzan;
import com.google.android.gms.maps.internal.zzap;
import com.google.android.gms.maps.internal.zzar;
import com.google.android.gms.maps.internal.zzat;
import com.google.android.gms.maps.internal.zzav;
import com.google.android.gms.maps.internal.zzax;
import com.google.android.gms.maps.internal.zzaz;
import com.google.android.gms.maps.internal.zzbb;
import com.google.android.gms.maps.internal.zzbd;
import com.google.android.gms.maps.internal.zzbf;
import com.google.android.gms.maps.internal.zzbh;
import com.google.android.gms.maps.internal.zzbu;
import com.google.android.gms.maps.internal.zzd;
import com.google.android.gms.maps.internal.zzi;
import com.google.android.gms.maps.internal.zzn;
import com.google.android.gms.maps.internal.zzp;
import com.google.android.gms.maps.internal.zzt;
import com.google.android.gms.maps.internal.zzv;
import com.google.android.gms.maps.internal.zzz;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;

import java.time.LocalDate;
import java.util.Calendar;

public class NoteActivity extends AppCompatActivity implements GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {

    Calendar c;
    DatePickerDialog dpd;

    Button button_from;
    Button button_before;
    Button button_location;
    Button button_deleteEvents;

    TextView textView_from;
    TextView textView_before;
    TextView textView_location;
    TextView textView_title;

    MultiAutoCompleteTextView multiAutoCompleteTextView;
    Dialog dialog;
    GoogleMap mMap;

    Menu menu_1;

    DBHelper dbHelper;

    SQLiteDatabase database;
    Cursor cursor;
    String id;

    public NoteActivity() {

    }

    public NoteActivity(String[] id, LocalDate selectedDate, int step) {

        dbHelper = new DBHelper(this);
        this.id = id[0];
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                this.id = null;
            } else {
                this.id = extras.getString("Id");
                dbHelper = new DBHelper(this);
            }
        } else {
            this.id = (String) savedInstanceState.getSerializable("Id");
            dbHelper = new DBHelper(this);
        }

        if(dbHelper == null){
            dbHelper = new DBHelper(this);

            initialActivity();
            setActivityEvents();

            getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Включаем отображение кнопки назад на панели ActionBar.
            // getActionBar().setHomeButtonEnabled(true);
        }else{
            initialActivity();
            setActivityEvents();

            getSupportActionBar().setDisplayHomeAsUpEnabled(true); // Включаем отображение кнопки назад на панели ActionBar.
            // getActionBar().setHomeButtonEnabled(true);

            database = dbHelper.getWritableDatabase();

            String selection = "id == ?";
            String[] selectionArgs = new String[] { id };
            //String select = "id = " + id[0];
            cursor = database.query(DBHelper.TABLE_CONTACT,null, selection, selectionArgs, null, null,null);

            if(cursor.moveToFirst()){
                int idindex = cursor.getColumnIndex(DBHelper.KEY_ID);
                int titleindex = cursor.getColumnIndex(DBHelper.KEY_TITLE);
                int locationindex = cursor.getColumnIndex(DBHelper.KEY_LOCATION);
                int descriptionindex = cursor.getColumnIndex(DBHelper.KEY_DESCRIPTION);
                int timefromindex = cursor.getColumnIndex(DBHelper.KEY_TIMEFROM);
                int timebeforeindex = cursor.getColumnIndex(DBHelper.KEY_TIMEBEFORE);

                textView_title.setText(cursor.getString(titleindex));
                textView_location.setText(cursor.getString(locationindex));
                multiAutoCompleteTextView.setText(cursor.getString(descriptionindex));
                textView_from.setText(cursor.getString(timefromindex));
                textView_before.setText(cursor.getString(timebeforeindex));

            }
            cursor.close();
        }

    }

    @Override
    public boolean onSupportNavigateUp() // Обработчик кнопки возврата.
    {
        System.out.println("Проверка 2");
        finish();
        return super.onSupportNavigateUp();
    }

    private void setActivityEvents() {

        button_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c= Calendar.getInstance();
                c.set(LocalDate.now().getYear(),LocalDate.now().getMonth().getValue(),LocalDate.now().getDayOfMonth());

                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(NoteActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        textView_from.setText(day + "/" + month + "/" + year);
                    }
                }, day, month, year);

                dpd.updateDate(LocalDate.now().getYear(),LocalDate.now().getMonth().getValue(),LocalDate.now().getDayOfMonth());
                dpd.show();
            }
        });

        button_before.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c= Calendar.getInstance();
                c.set(LocalDate.now().getYear(),LocalDate.now().getMonth().getValue(),LocalDate.now().getDayOfMonth());

                int day = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(NoteActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int day) {
                        textView_before.setText(day + "/" + month + "/" + year);
                    }
                }, day, month, year);

                dpd.updateDate(LocalDate.now().getYear(),LocalDate.now().getMonth().getValue(),LocalDate.now().getDayOfMonth());
                dpd.show();
            }
        });

        button_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(NoteActivity.this);

                // Установите заголовок
                dialog.setTitle("Заголовок диалога");
                // Передайте ссылку на разметку
                dialog.setContentView(R.layout.activity_maps);//dialog_view

                //dialog.get
                // Найдите элемент TextView внутри вашей разметки
                // и установите ему соответствующий текст
                //TextView text = (TextView) dialog.findViewById(R.id.locat);
                //text.setText("Текст в диалоговом окне. Вы любите котов?");
                dialog.show();
                dialog.invalidateOptionsMenu();

                Button button2 = (Button)dialog.findViewById(R.id.button2);

                Button button_1 = (Button)dialog.findViewById(R.id.button_1);

                IGoogleMapDelegate iGoogleMapDelegate = new IGoogleMapDelegate() {

                    @Override
                    public CameraPosition getCameraPosition() throws RemoteException {
                        return null;
                    }

                    @Override
                    public float getMaxZoomLevel() throws RemoteException {
                        return 0;
                    }

                    @Override
                    public float getMinZoomLevel() throws RemoteException {
                        return 0;
                    }

                    @Override
                    public void moveCamera(@NonNull  IObjectWrapper iObjectWrapper) throws RemoteException {

                    }

                    @Override
                    public void animateCamera(@NonNull  IObjectWrapper iObjectWrapper) throws RemoteException {

                    }

                    @Override
                    public void stopAnimation() throws RemoteException {

                    }

                    @Override
                    public void clear() throws RemoteException {

                    }

                    @Override
                    public int getMapType() throws RemoteException {
                        return 0;
                    }

                    @Override
                    public void setMapType(int i) throws RemoteException {

                    }

                    @Override
                    public boolean isTrafficEnabled() throws RemoteException {
                        return false;
                    }

                    @Override
                    public void setTrafficEnabled(boolean b) throws RemoteException {

                    }

                    @Override
                    public boolean isIndoorEnabled() throws RemoteException {
                        return false;
                    }

                    @Override
                    public boolean setIndoorEnabled(boolean b) throws RemoteException {
                        return false;
                    }

                    @Override
                    public boolean isMyLocationEnabled() throws RemoteException {
                        return false;
                    }

                    @Override
                    public void setMyLocationEnabled(boolean b) throws RemoteException {

                    }


                    @Override
                    public Location getMyLocation() throws RemoteException {
                        return null;
                    }

                    @Override
                    public void setLocationSource( ILocationSourceDelegate iLocationSourceDelegate) throws RemoteException {

                    }


                    @Override
                    public IUiSettingsDelegate getUiSettings() throws RemoteException {
                        return null;
                    }


                    @Override
                    public IProjectionDelegate getProjection() throws RemoteException {
                        return null;
                    }

                    @Override
                    public void setPadding(int i, int i1, int i2, int i3) throws RemoteException {

                    }

                    @Override
                    public boolean isBuildingsEnabled() throws RemoteException {
                        return false;
                    }

                    @Override
                    public void setBuildingsEnabled(boolean b) throws RemoteException {

                    }

                    @Override
                    public void setWatermarkEnabled(boolean b) throws RemoteException {

                    }

                    @Override
                    public void onCreate(@NonNull  Bundle bundle) throws RemoteException {

                    }

                    @Override
                    public void onResume() throws RemoteException {

                    }

                    @Override
                    public void onPause() throws RemoteException {

                    }

                    @Override
                    public void onDestroy() throws RemoteException {

                    }

                    @Override
                    public void onLowMemory() throws RemoteException {

                    }

                    @Override
                    public boolean useViewLifecycleWhenInFragment() throws RemoteException {
                        return false;
                    }

                    @Override
                    public void onSaveInstanceState(@NonNull  Bundle bundle) throws RemoteException {

                    }

                    @Override
                    public void setContentDescription( String s) throws RemoteException {

                    }

                    @Override
                    public void onEnterAmbient(@NonNull  Bundle bundle) throws RemoteException {

                    }

                    @Override
                    public void onExitAmbient() throws RemoteException {

                    }

                    @Override
                    public void setMinZoomPreference(float v) throws RemoteException {

                    }

                    @Override
                    public void setMaxZoomPreference(float v) throws RemoteException {

                    }

                    @Override
                    public void resetMinMaxZoomPreference() throws RemoteException {

                    }

                    @Override
                    public void setLatLngBoundsForCameraTarget( LatLngBounds latLngBounds) throws RemoteException {

                    }

                    @Override
                    public boolean setMapStyle( MapStyleOptions mapStyleOptions) throws RemoteException {
                        return false;
                    }

                    @Override
                    public void onStart() throws RemoteException {

                    }

                    @Override
                    public void onStop() throws RemoteException {

                    }

                    @Override
                    public void animateCameraWithCallback(IObjectWrapper iObjectWrapper,  zzd zzd) throws RemoteException {

                    }

                    @Override
                    public void animateCameraWithDurationAndCallback(IObjectWrapper iObjectWrapper, int i, zzd zzd) throws RemoteException {

                    }

                    @Override
                    public zzad addPolyline(PolylineOptions polylineOptions) throws RemoteException {
                        return null;
                    }

                    @Override
                    public zzaa addPolygon(PolygonOptions polygonOptions) throws RemoteException {
                        return null;
                    }

                    @Override
                    public zzx addMarker(MarkerOptions markerOptions) throws RemoteException {
                        return null;
                    }

                    @Override
                    public zzo addGroundOverlay(GroundOverlayOptions groundOverlayOptions) throws RemoteException {
                        return null;
                    }

                    @Override
                    public zzag addTileOverlay(TileOverlayOptions tileOverlayOptions) throws RemoteException {
                        return null;
                    }

                    @Override
                    public void setOnCameraChangeListener(zzn zzn) throws RemoteException {

                    }

                    @Override
                    public void setOnMapClickListener(zzal zzal) throws RemoteException {

                    }

                    @Override
                    public void setOnMapLongClickListener(zzap zzap) throws RemoteException {

                    }

                    @Override
                    public void setOnMarkerClickListener(zzat zzat) throws RemoteException {

                    }

                    @Override
                    public void setOnMarkerDragListener( zzav zzav) throws RemoteException {

                    }

                    @Override
                    public void setOnInfoWindowClickListener(com.google.android.gms.maps.internal.zzad zzad) throws RemoteException {

                    }

                    @Override
                    public void setInfoWindowAdapter(zzi zzi) throws RemoteException {

                    }

                    @Override
                    public zzl addCircle(CircleOptions circleOptions) throws RemoteException {
                        return null;
                    }

                    @Override
                    public void setOnMyLocationChangeListener(zzaz zzaz) throws RemoteException {

                    }

                    @Override
                    public void setOnMyLocationButtonClickListener(zzax zzax) throws RemoteException {

                    }

                    @Override
                    public void snapshot(zzbu zzbu,  IObjectWrapper iObjectWrapper) throws RemoteException {

                    }

                    @Override
                    public void setOnMapLoadedCallback(zzan zzan) throws RemoteException {

                    }

                    @Override
                    public zzr getFocusedBuilding() throws RemoteException {
                        return null;
                    }

                    @Override
                    public void setOnIndoorStateChangeListener(zzab zzab) throws RemoteException {

                    }

                    @Override
                    public void getMapAsync(zzar zzar) throws RemoteException {

                    }

                    @Override
                    public void snapshotForTest(zzbu zzbu) throws RemoteException {

                    }

                    @Override
                    public void setOnPoiClickListener( zzbd zzbd) throws RemoteException {

                    }

                    @Override
                    public void setOnGroundOverlayClickListener( zzz zzz) throws RemoteException {

                    }

                    @Override
                    public void setOnInfoWindowLongClickListener( zzah zzah) throws RemoteException {

                    }

                    @Override
                    public void setOnPolygonClickListener( zzbf zzbf) throws RemoteException {

                    }

                    @Override
                    public void setOnInfoWindowCloseListener( zzaf zzaf) throws RemoteException {

                    }

                    @Override
                    public void setOnPolylineClickListener( zzbh zzbh) throws RemoteException {

                    }

                    @Override
                    public void setOnCircleClickListener( com.google.android.gms.maps.internal.zzx zzx) throws RemoteException {

                    }

                    @Override
                    public void setOnCameraMoveStartedListener( zzv zzv) throws RemoteException {

                    }

                    @Override
                    public void setOnCameraMoveListener( zzt zzt) throws RemoteException {

                    }

                    @Override
                    public void setOnCameraMoveCanceledListener( com.google.android.gms.maps.internal.zzr zzr) throws RemoteException {

                    }

                    @Override
                    public void setOnCameraIdleListener( zzp zzp) throws RemoteException {

                    }

                    @Override
                    public void setOnMyLocationClickListener( zzbb zzbb) throws RemoteException {

                    }

                    @Override
                    public IBinder asBinder() {
                        return null;
                    }
                };
                mMap = new GoogleMap(iGoogleMapDelegate);

                SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView_1);

                final String[] coordinats = new String[1];
                supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(@NonNull  GoogleMap googleMap) {
                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(@NonNull  LatLng latLng) {
                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                markerOptions.title(latLng.latitude + ":" + latLng.longitude);
                                coordinats[0] = latLng.latitude + ":" + latLng.longitude;
                                googleMap.clear();
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                                googleMap.addMarker(markerOptions);
                            }
                        });
                    }
                });

                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                button_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        textView_location.setText(coordinats[0]);
                        dialog.dismiss();
                    }
                });
            }
        });

        button_deleteEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selection = "id == ?";
                String[] selectionArgs = new String[] { id };
                //String select = "id = " + id[0];

                database.delete(DBHelper.TABLE_CONTACT, selection, selectionArgs );
                //cursor = database.query(DBHelper.TABLE_CONTACT,null, selection, selectionArgs, null, null,null);
                finish();
            }
        });

    }

    private void initialActivity() {

        button_from = (Button)this.findViewById(R.id.button_from);
        button_before = (Button)this.findViewById(R.id.button_before);

        button_location = (Button)this.findViewById(R.id.button_location);

        button_deleteEvents = (Button)this.findViewById(R.id.Delete_Events);

        textView_from = (TextView) this.findViewById(R.id.textView_from);
        textView_before = (TextView) this.findViewById(R.id.textView_before);

        textView_location = (TextView) this.findViewById(R.id.location);

        textView_title = (TextView) this.findViewById(R.id.Title);

        multiAutoCompleteTextView = (MultiAutoCompleteTextView)this.findViewById(R.id.multiAutoCompleteTextView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note, menu); // Привязка к MainActivity виджета nemu.
        //tick_1 = findViewById(R.id.ti);
        menu_1 = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case(R.id.tick_1): // Если выбран item - смена режима.

                database = dbHelper.getWritableDatabase();

                ContentValues contentValues = new ContentValues();

                contentValues.put(DBHelper.KEY_TITLE, (String) "" + textView_title.getText());
                contentValues.put(DBHelper.KEY_LOCATION, (String) "" + textView_location.getText());
                contentValues.put(DBHelper.KEY_DESCRIPTION, (String) "" + multiAutoCompleteTextView.getText());
                contentValues.put(DBHelper.KEY_TIMEFROM, (String) "" + textView_from.getText());
                contentValues.put(DBHelper.KEY_TIMEBEFORE, (String) "" + textView_before.getText());

                database.insert(DBHelper.TABLE_CONTACT, null, contentValues);

                /*
                * SQLiteDatabase:
                * query() - Для чтения данных из БД.
                * insert() - Для добавления данных в БД.
                * delete() - Для удаления данных из БД.
                * update() - Для изменение данных в БД.
                * */
                System.out.println("print _ 1");
                System.out.println(textView_title.getText());
                System.out.println(textView_location.getText());
                System.out.println(multiAutoCompleteTextView.getText());
                System.out.println(textView_from.getText());
                System.out.println(textView_before.getText());

                // Сохранение в БД.

                //

                this.finish();

              return true;

            default:
                System.out.println("Проверка 2");
                finish();
                return true;
        }
    }

    @Override
    public void onMapClick(@NonNull  LatLng latLng) {
        System.out.println("Hello 1");

    }

    @Override
    public void onMapLongClick(@NonNull  LatLng latLng) {
        System.out.println("Hello 2");
    }
}