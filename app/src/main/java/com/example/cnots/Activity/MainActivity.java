package com.example.cnots.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.cnots.Adapters.PageAdapter;
import com.example.cnots.Fragments.Fragment_Day;
import com.example.cnots.Fragments.Fragment_Month;
import com.example.cnots.Fragments.Fragment_Year;
import com.example.cnots.Interfase.OnItemListener;
import com.example.cnots.R;
import com.example.cnots.SQLite.DBHelper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * MainActivity - Основное activity программы.
 * @author minee <mineeff20@yandex.ru>
 * @version 1.0.0
 * @Ot
 */

/** Поле _ _ _
 *
 * Возможные состояния:
 *  0 - . . .;
 *  1 - . . .;
 *  2 - . . .;
 * */

public class MainActivity extends AppCompatActivity implements OnItemListener {

    /** Поле ФРАГМЕНТ_ФЛАГ
     * @deprecated {Хранит состояние выбраного фрагмента.}
     * Возможные состояния:
     *  0 - Fragment_Month;
     *  1 - Fragment_Year;
     *  2 - Fragment_Day;
     * */
    private int FRAGMENT_FLAG = 0;

    /** Поте item R.id.goToTodayButton in menu. */
    private MenuItem goToTodayButton = null;

    /** Поте текущего меню
     * @deprecated {Хранит состояние текущего меню.}
     * */
    private Menu menu_1;

    /** Поле текущего  ViewPager2
     * @deprecated {Хранит текущий ViewPager2}
     * */
    private ViewPager View_Pager;

    /** Поле _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    private PageAdapter P_Adapter;

    /** Поле _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    private int lastPosition = 0;

    /** Поле _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    List<Fragment> list = new ArrayList<>();

    DBHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    Cursor cursor;

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);
        sqLiteDatabase = dbHelper.getWritableDatabase();

        Update_Month_View();
        //Update_Year_View();
        //Update_Day_View();

        FloatingActionButton f_button_1 = findViewById(R.id.floatingActionButton2);

        f_button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NoteActivity.class);
                startActivity(intent);
            }
        });
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    private void Update_Year_View(){

        lastPosition = 0;

        list.add(new Fragment_Year(-1));    // -1 - Предыдущий год.
        list.add(new Fragment_Year(0));     // 0 - Текущий Год.
        list.add(new Fragment_Year(1));     // 1 - Последующий год.
        View_Pager = findViewById(R.id.view_pager_1);
        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1); // setCurrentItem(int) - Должен быть определен после setAdapter(PageAdapter) и обязательно должен быть вызван метод invalidate().
        View_Pager.invalidate();

       // P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
        //View_Pager.setPageTransformer(new PageTransformer());
        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                System.out.println("onPageSelected_1_Y");

                if ((lastPosition >= position)) {
                    System.out.println("Right");
                    //System.out.println(lastPosition);
                    //System.out.println(position);

                    if((lastPosition == position) && (position == 1) ){

                    }else{
                        System.out.println("Right_2");
                        //previousMonthAction();
                        previousYearAction();
                    }
                }else if (lastPosition < position) {
                    System.out.println("Left");
                    //System.out.println(position);

                    //nextMonthAction();
                    nextYearAction();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        //View_Pager.setCurrentItem(1);
        //View_Pager.setAdapter(P_Adapter);
        //View_Pager.setCurrentItem(1);
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.out.println("onDestroy(): ");
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    @Override
    protected void onRestart() {
        super.onRestart();

        if(dbHelper == null){
            dbHelper = new DBHelper(this);

        }else{

        }
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        cursor = database.query(DBHelper.TABLE_CONTACT, null, null, null, null, null, null);

        this.recreate();

        System.out.println("onRestart(): ");
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    @Override
    protected void onPause() {
        super.onPause();

        System.out.println("onPause(): ");
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    @Override
    protected void onResume() {
        super.onResume();

        System.out.println("onResume(): ");
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    void Update_Month_View() {

        list.add(new Fragment_Month(-1));// -1 - Предыдущий месяц.
        list.add(new Fragment_Month(0));// 0 - Текущий месяц.
        list.add(new Fragment_Month(1));// 1 - Последующий месяц.

        View_Pager = findViewById(R.id.view_pager_1);

        if (View_Pager.getAdapter() != null)
            View_Pager.setAdapter(null);
        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
        lastPosition = 0;
        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1);

        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            System.out.println("onPageSelected_1");

            if ((lastPosition >= position)) {
                System.out.println("Right");
                //System.out.println(lastPosition);
                //System.out.println(position);

                if((lastPosition == position) && (position == 1) ){

                }else{
                    System.out.println("Right_2");
                    previousMonthAction();
                }
            }else if (lastPosition < position) {
                System.out.println("Left");
                //System.out.println(position);

                nextMonthAction();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    });
    }

    /** Метод _ _ _
     * @deprecated {Обработчик событий меню.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu); // Привязка к MainActivity виджета nemu.
        goToTodayButton = findViewById(R.id.go_to_days);
        menu_1 = menu;
        return super.onCreateOptionsMenu(menu);
    }

    /** Метод _ _ _
     * @deprecated {Описание: Обработчик нажатия кнопок меню.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case(R.id.go_to_days): // Если выбран item - смена режима.

                list.clear();
                View_Pager.clearOnPageChangeListeners();

                switch(FRAGMENT_FLAG) // Переключение между режимами.
                {
                    case (0):

                        Update_Year_View();

                        /*
                        FragmentManager F_Manager = getSupportFragmentManager();
                        FragmentTransaction F_Transactuion = F_Manager.beginTransaction();
                        F_Transactuion.replace(R.id.fragments_holder, fragment_month = new Fragment_Month());
                        F_Transactuion.commit();
                        */

                        FRAGMENT_FLAG++;
                        return true;

                    case (2):

                        Update_Month_View();


                        FRAGMENT_FLAG = 0;

                        return true;

                    case (1):

                        Update_Day_View();

                        FRAGMENT_FLAG++;

                        return true;
                }

                return true;
            case (R.id.about):
                Intent intent_1 = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intent_1);
                return true;
            case (R.id.settings):
                Intent intent_2 = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent_2);
                return true;
            case(R.id.exit):
                System.exit(0);
                return true;
            case(R.id.delete_events):
                sqLiteDatabase.delete(DBHelper.TABLE_CONTACT,null,null);
                this.recreate();
                //this.onNavigateUp();

                //View_Pager.notify();
                //View_Pager.clearOnPageChangeListeners();
                //cursor = sqLiteDatabase.query(DBHelper.TABLE_CONTACT, null, null, null, null, null, null);
                //P_Adapter = new PageAdapter(getSupportFragmentManager() ,list);
                //View_Pager.setAdapter(P_Adapter);
                //View_Pager.invalidate();
                /*
                View view;
                if ((view = findViewById(R.id.view_pager_1)) != null) {
                    view.setVisibility(View.VISIBLE);
                    view.invalidate();
                }*/
                return true;
        }
        return true;
    }

    private void Update_Day_View() {

        list.add(new Fragment_Day(-1));// -1 - Предыдущий день.
        list.add(new Fragment_Day(0));// 0 - Текущий день.
        list.add(new Fragment_Day(+1));// 1 - Последующий день.
        lastPosition = 0;

        View_Pager = findViewById(R.id.view_pager_1);

        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);

        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1); // setCurrentItem(int) - Должен быть определен после setAdapter(PageAdapter) и обязательно должен быть вызван метод invalidate().
        View_Pager.invalidate();

        //View_Pager.setPageTransformer(new PageTransformer());
        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                System.out.println("onPageSelected_3_Y");

                if ((lastPosition >= position)) {
                    System.out.println("Right");
                    //System.out.println(lastPosition);
                    //System.out.println(position);

                    if((lastPosition == position) && (position == 1) ){

                    }else{
                        System.out.println("Right");
                        previousDayAction();
                    }
                }else if (lastPosition < position) {
                    System.out.println("left");
                    nextDayAction();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    private void nextMonthAction() {
        lastPosition = 1;
        list.add(new Fragment_Month(((Fragment_Month)list.get(list.size() - 1)).get_step() + 1));
        list.remove(0);
        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1); // Работает для 1 итерации
        View_Pager.invalidate();

        //View_Pager.setCurrentItem(1);
/*
        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            long currentTime = 0;

            @Override
            public void onPageSelected(int position) {

                System.out.println("onPageSelected_2");

                if (lastPosition > position) {
                    System.out.println("Right");
                    System.out.println(position);
                    //fun_Month(position);

                    //previousMonthAction();

                }else if (lastPosition < position) {
                    System.out.println("Left");
                    System.out.println(position);
                    //fun_Month(position);

                    nextMonthAction();



                    //nextMonthAction();
                }
                lastPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

*/
        //View_Pager.invalidate();
/*
        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            long currentTime = 0;

            @Override
            public void onPageSelected(int position) {

                if (lastPosition > position) {
                    System.out.println("Right");
                    System.out.println(position);
                    //fun_Month(position);

                    //previousMonthAction();


                }else if (lastPosition < position) {
                    System.out.println("Left");
                    System.out.println(position);
                    //fun_Month(position);
                    if(currentTime == 0){
                        currentTime = Calendar.getInstance().getTimeInMillis();
                        nextMonthAction();
                    }

                    if(((Calendar.getInstance().getTimeInMillis() - currentTime) > 1)){
                        System.out.println("Timer_1_1: ");
                        System.out.println((Calendar.getInstance().getTimeInMillis() - currentTime));
                        currentTime = Calendar.getInstance().getTimeInMillis();
                        System.out.println((Calendar.getInstance().getTimeInMillis() - currentTime));
                    }else{
                        System.out.println("Timer_2_2: ");
                        System.out.println((Calendar.getInstance().getTimeInMillis() - currentTime));
                        currentTime = Calendar.getInstance().getTimeInMillis();
                        System.out.println((Calendar.getInstance().getTimeInMillis() - currentTime));
                        nextMonthAction();
                    }

                    //nextMonthAction();
                }
                lastPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
 */
        //View_Pager.setCurrentItem(((int) View_Pager.getCurrentItem()) + 1);
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    private void nextYearAction() {
        lastPosition = 1;
        list.add(new Fragment_Year(((Fragment_Year)list.get(list.size() - 1)).get_step() + 1));
        list.remove(0);
        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1); // Работает для 1 итерации
        View_Pager.invalidate();

        //View_Pager.setCurrentItem(1);
/*
        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            long currentTime = 0;

            @Override
            public void onPageSelected(int position) {

                System.out.println("onPageSelected_2");

                if (lastPosition > position) {
                    System.out.println("Right");
                    System.out.println(position);
                    //fun_Month(position);

                    //previousMonthAction();

                }else if (lastPosition < position) {
                    System.out.println("Left");
                    System.out.println(position);
                    //fun_Month(position);

                    nextMonthAction();



                    //nextMonthAction();
                }
                lastPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

*/
        //View_Pager.invalidate();
/*
        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            long currentTime = 0;

            @Override
            public void onPageSelected(int position) {

                if (lastPosition > position) {
                    System.out.println("Right");
                    System.out.println(position);
                    //fun_Month(position);

                    //previousMonthAction();


                }else if (lastPosition < position) {
                    System.out.println("Left");
                    System.out.println(position);
                    //fun_Month(position);
                    if(currentTime == 0){
                        currentTime = Calendar.getInstance().getTimeInMillis();
                        nextMonthAction();
                    }

                    if(((Calendar.getInstance().getTimeInMillis() - currentTime) > 1)){
                        System.out.println("Timer_1_1: ");
                        System.out.println((Calendar.getInstance().getTimeInMillis() - currentTime));
                        currentTime = Calendar.getInstance().getTimeInMillis();
                        System.out.println((Calendar.getInstance().getTimeInMillis() - currentTime));
                    }else{
                        System.out.println("Timer_2_2: ");
                        System.out.println((Calendar.getInstance().getTimeInMillis() - currentTime));
                        currentTime = Calendar.getInstance().getTimeInMillis();
                        System.out.println((Calendar.getInstance().getTimeInMillis() - currentTime));
                        nextMonthAction();
                    }

                    //nextMonthAction();
                }
                lastPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


 */
        //View_Pager.setCurrentItem(((int) View_Pager.getCurrentItem()) + 1);
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    private void previousMonthAction() {
       // FLAG[0] = true;
        lastPosition = 1;
        list.add(0, new Fragment_Month(((Fragment_Month)list.get(0)).get_step() - 1));
        list.remove(list.size() - 1);
        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
        //View_Pager.setCurrentItem(1);
        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1);
        View_Pager.invalidate();
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    private void previousYearAction() {
        // FLAG[0] = true;
        lastPosition = 1;
        list.add(0, new Fragment_Year(((Fragment_Year)list.get(0)).get_step() - 1));
        list.remove(list.size() - 1);
        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
        //View_Pager.setCurrentItem(1);
        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1);
        View_Pager.invalidate();
    }

    /** Метод _ _ _
     * @deprecated {Описание: Запускается при нажатии кнопки prev.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    public void previousMonthAction(View view) {

        previousMonthAction();
    }

    /** Метод _ _ _
     * @deprecated {Описание: Запускается при нажатии кнопки next.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    public void nextMonthAction(View view) {

        nextMonthAction();
    }

    /** Метод _ _ _
     * @deprecated {Описание.}
     * Возможные состояния:
     *  0 - . . .;
     *  1 - . . .;
     *  2 - . . .;
     * */
    @Override
    public void onItemClick(int position, String Day_Text) {
        if(Day_Text.equals("")){
            String  message = "Selected Date" + Day_Text + "  ";
            Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
        }
    }

    public void previousDayAction(View view) {
        previousDayAction();
    }

    private void previousDayAction() {
        lastPosition = 1;
        list.add(0, new Fragment_Day(((Fragment_Day)list.get(0)).get_step() - 1));
        list.remove(list.size() - 1);
        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
        //View_Pager.setCurrentItem(1);
        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1);
        View_Pager.invalidate();
    }

    public void nextDayAction(View view) {

        nextDayAction();
    }

    private void nextDayAction() {
        lastPosition = 1;
        list.add(new Fragment_Day(((Fragment_Day)list.get(list.size() - 1)).get_step() + 1));
        list.remove(0);
        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);
       // P_Adapter.notifyDataSetChanged();
        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1); // Работает для 1 итерации
        View_Pager.invalidate();
    }

    public void InsertDay(LocalDate localdate) {
        list.clear();
        View_Pager.clearOnPageChangeListeners();
        list.add(new Fragment_Day(localdate.minusDays(1), ChronoUnit.DAYS.between(LocalDate.now(), (localdate.minusDays(1)))));// -1 - Предыдущий день.
        list.add(new Fragment_Day(localdate, LocalDate.now().getDayOfYear() -localdate.getDayOfYear()));// 0 - Текущий день.
        list.add(new Fragment_Day(localdate.plusDays(1), ChronoUnit.DAYS.between(LocalDate.now(), (localdate.plusDays(1)))));// 1 - Последующий день.
        lastPosition = 0;

        View_Pager = findViewById(R.id.view_pager_1);

        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);

        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1); // setCurrentItem(int) - Должен быть определен после setAdapter(PageAdapter) и обязательно должен быть вызван метод invalidate().
        View_Pager.invalidate();

        //View_Pager.setPageTransformer(new PageTransformer());
        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                System.out.println("onPageSelected_3_Y");

                if ((lastPosition >= position)) {
                    System.out.println("Right");
                    //System.out.println(lastPosition);
                    //System.out.println(position);

                    if((lastPosition == position) && (position == 1) ){

                    }else{
                        System.out.println("Right");
                        previousDayAction();
                    }
                }else if (lastPosition < position) {
                    System.out.println("left");
                    nextDayAction();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void InsertMonth(LocalDate localdate) {

        list.clear();
        View_Pager.clearOnPageChangeListeners();
        list.add(new Fragment_Month(localdate.minusMonths(1), ChronoUnit.MONTHS.between(LocalDate.now(), (localdate.minusMonths(1)))));// -1 - Предыдущий день.
        list.add(new Fragment_Month(localdate, LocalDate.now().getDayOfYear() -localdate.getDayOfYear()));// 0 - Текущий день.
        list.add(new Fragment_Month(localdate.plusMonths(1), ChronoUnit.MONTHS.between(LocalDate.now(), (localdate.plusMonths(1)))));// 1 - Последующий день.
        lastPosition = 0;

        View_Pager = findViewById(R.id.view_pager_1);

        P_Adapter = new PageAdapter(getSupportFragmentManager(), list);

        View_Pager.setAdapter(P_Adapter);
        View_Pager.setCurrentItem(1); // setCurrentItem(int) - Должен быть определен после setAdapter(PageAdapter) и обязательно должен быть вызван метод invalidate().
        View_Pager.invalidate();

        //View_Pager.setPageTransformer(new PageTransformer());
        View_Pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                System.out.println("onPageSelected_3_Y");

                if ((lastPosition >= position)) {
                    System.out.println("Right");
                    //System.out.println(lastPosition);
                    //System.out.println(position);

                    if((lastPosition == position) && (position == 1) ){

                    }else{
                        System.out.println("Right");
                        previousMonthAction();
                    }
                }else if (lastPosition < position) {
                    System.out.println("left");
                    nextMonthAction();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void InsertDay(String[] id, LocalDate selectedDate, int step) {

        //NoteActivity noteActivity = new NoteActivity(id, selectedDate, step);
        /*
        При запуске активити через Intent можно передавать ей данные вида "ключ-значение". В запускаемой активити их можно получить и обработать.
                Главная активити:
        */

        Intent intent = new Intent(MainActivity.this, NoteActivity.class);
        intent.putExtra("Id", id[0]);
        intent.putExtra("selectedDate", selectedDate);
        intent.putExtra("step", step);
        startActivity(intent);
    }
}