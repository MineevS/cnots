package com.example.cnots.extensions;

import android.view.View;
import android.widget.Toast;

import androidx.viewpager2.widget.ViewPager2;

public class PageTransformer implements ViewPager2.PageTransformer {
    public void transformPage(View view, float position) {

        if (position < -1) {
            // [-00,-1): the page is way off-screen to the left.
            Toast.makeText(view.getContext(),"left", Toast.LENGTH_SHORT).show();
            //System.out.println("left");
        } else if (position <= 1) {
            // [-1,1]: the page is "centered"
        } else {
            Toast.makeText(view.getContext(),"right", Toast.LENGTH_SHORT).show();
           // System.out.println("Right");
            // (1,+00]: the page is way off-screen to the right.
        }
    }
}