package com.example.cnots.Holders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cnots.Interfase.OnItemListener;
import com.example.cnots.R;

public class Calendar_Holder extends RecyclerView.ViewHolder implements  View.OnClickListener {

    public final TextView DayOfMonth;
    private final OnItemListener onItemListener;

    public Calendar_Holder(@NonNull View itemView, OnItemListener onItemListener) {
        super(itemView);
        this.DayOfMonth = itemView.findViewById(R.id.cellDayText);
        this.onItemListener = onItemListener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.onItemListener.onItemClick(getBindingAdapterPosition() , (String) DayOfMonth.getText());
    }
}
