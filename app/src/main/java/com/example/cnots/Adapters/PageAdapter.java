package com.example.cnots.Adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.cnots.Fragments.Fragment_Month;

import java.util.List;

public class PageAdapter extends FragmentStatePagerAdapter /* FragmentPagerAdapter FragmentStateAdapter*/{

    FragmentManager _fragmentManager;
    List<Fragment> _list; // Храним список Fragments;


    public PageAdapter(FragmentManager supportFragmentManager, List<Fragment> list) {
        super(supportFragmentManager);
        this._fragmentManager = supportFragmentManager;
        this._list = list;
    }

/*
    @Override
    public Fragment createFragment(int position) {
        return _list.get(position);
    }

    @Override
    public int getItemCount() {
        return _list.size();
    }
    */


    @Override
    public Fragment getItem(int position) {
        return _list.get(position);
    }

    @Override
    public int getCount() {
        return _list.size();
    }
}
