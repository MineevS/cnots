package com.example.cnots.Adapters;

import android.graphics.Color;
import android.icu.util.LocaleData;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cnots.Holders.Calendar_Holder;
import com.example.cnots.Interfase.OnItemListener;
import com.example.cnots.R;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Random;

public class Calendar_Adapter extends RecyclerView.Adapter<Calendar_Holder> {

    private final ArrayList<String> DayOfMonth;
    private final OnItemListener onItemListener;
    private LocalDate selectedDate = null;

    public Calendar_Adapter(ArrayList<String> dayOfMonth, OnItemListener onItemListener) {
        this.DayOfMonth = dayOfMonth;
        this.onItemListener = onItemListener;
    }

    public Calendar_Adapter(ArrayList<String> dayOfMonth, OnItemListener onItemListener, LocalDate selectedDate) {
        this.DayOfMonth = dayOfMonth;
        this.onItemListener = onItemListener;
        this.selectedDate = selectedDate;
    }

    @Override
    public Calendar_Holder onCreateViewHolder(@NonNull  ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.calendar_cell, parent, false);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = (int) (parent.getHeight() * 0.166666666);
        return new Calendar_Holder(view, onItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull Calendar_Holder holder, int position) {

        try{
            if((selectedDate != null) && (selectedDate.getDayOfMonth() == Integer.parseInt(DayOfMonth.get(position))))
            {
                holder.DayOfMonth.setText(DayOfMonth.get(position));
                //holder.itemView.setAlpha(100.1f);
                holder.itemView.setBackgroundColor(Color.LTGRAY);
            }else{
                holder.DayOfMonth.setText(DayOfMonth.get(position));

            }
        }
        catch (NumberFormatException ex)
        {
            //Random rand = new Random();
            //float r = rand.nextFloat();
            //float g = rand.nextFloat();
            //float b = rand.nextFloat();

            holder.DayOfMonth.setText(DayOfMonth.get(position));
            //holder.itemView.setBackgroundColor(Color.rgb(r,g,b));
        }
    }

    @Override
    public int getItemCount() {
        return DayOfMonth.size();
    }
}
