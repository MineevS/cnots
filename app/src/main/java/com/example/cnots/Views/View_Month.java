package com.example.cnots.Views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.icu.util.LocaleData;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.example.cnots.Activity.ListNoteActivity;
import com.example.cnots.Interfase.OnItemListener;
import com.example.cnots.R;

import java.time.LocalDate;

/**
 * TODO: document your custom view class.
 */
public class View_Month extends View {
    private int _month = 0;
    private LocalDate selectedDate = null;
    private String mExampleString = " "; // TODO: use a default from R.string...
    private int mExampleColor = Color.RED; // TODO: use a default from R.color...
    private float mExampleDimension = 0; // TODO: use a default from R.dimen...
    private Drawable mExampleDrawable;

    private TextPaint mTextPaint;
    private float mTextWidth;
    private float mTextHeight;
    ////////


    public void setSelectedDate(LocalDate selectedDate) {
        this.selectedDate = selectedDate;
    }

    public LocalDate getSelectedDate() {
        return selectedDate;
    }

    ////////////////
    int _start = 1;
    int _end = 31;

    public void set_start(int _start) {
        this._start = _start;
    }

    public int get_start() {
        return _start;
    }

    public void set_end(int _end) {
        this._end = _end;
    }

    public int get_end() {
        return _end;
    }
    ///////////////

    public View_Month(Context context) {
        super(context);
        init(null, 0);
    }

    public View_Month(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public View_Month(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.View_Month, defStyle, 0);

        mExampleString = a.getString(
                R.styleable.View_Month_exampleString);
        mExampleColor = a.getColor(
                R.styleable.View_Month_exampleColor,
                mExampleColor);
        // Use getDimensionPixelSize or getDimensionPixelOffset when dealing with
        // values that should fall on pixel boundaries.
        mExampleDimension = a.getDimension(
                R.styleable.View_Month_exampleDimension,
                mExampleDimension);

        if (a.hasValue(R.styleable.View_Month_exampleDrawable)) {
            mExampleDrawable = a.getDrawable(
                    R.styleable.View_Month_exampleDrawable);
            mExampleDrawable.setCallback(this);
        }

        a.recycle();

        // Set up a default TextPaint object
        mTextPaint = new TextPaint();
        mTextPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextAlign(Paint.Align.LEFT);

        // Update TextPaint and text measurements from attributes
        invalidateTextPaintAndMeasurements();
    }

    private void invalidateTextPaintAndMeasurements() {
        mTextPaint.setTextSize(mExampleDimension);
        mTextPaint.setColor(mExampleColor);
        mTextPaint.setTextSize(mTextPaint.getTextSize() - 35);

        mExampleString = (String) getResources().getText(R.string.message);

        mTextWidth = mTextPaint.measureText(mExampleString);

        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        mTextHeight = fontMetrics.bottom;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);



        int start = _start;
        int end = _end + 1;
        Integer cursor = _start + 1;

        // TODO: consider storing these as member variables to reduce
        // allocations per draw cycle.
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

       for(int y = 1; y < 7; y++){

           for(int x = 1; x <= 7; x++)
           {
               if(cursor <= 0)
               {
                   canvas.drawText("", x * (paddingLeft / 8  +  contentWidth / 7) -(5.1f)* (getWidth()/42), y * (paddingTop / 6 + contentHeight / 8) , mTextPaint);
               }else if(cursor < end)
               {
                   canvas.drawText(cursor.toString(), x * (paddingLeft / 8  +  contentWidth / 7) - (5.1f)*(getWidth()/42), y * (paddingTop / 6 + contentHeight /8) , mTextPaint);
               }

               if((selectedDate != null) && (cursor == selectedDate.getDayOfMonth())){//&& (cursor == selectedDate.getDayOfMonth())

                   Paint paint = new Paint();
                   paint.setStyle(Paint.Style.FILL);
                   //canvas.drawColor(Color.RED);
                   paint.setColor(Color.rgb(255, 165, 0));
                   paint.setAlpha(127);
                   /*
                   * 255 - Полностью не прозрачный.
                   * 127 - средне прозрачный.
                   * 0 - Полностью прозрачный.
                   * */
                   canvas.drawCircle(x * (paddingLeft / 8  +  contentWidth / 7) - (5.5f)*(getWidth()/66) , y * (paddingTop / 6 + contentHeight /8) - 5, getWidth() /20, paint);
               }

               cursor++;
           }
       }
        // Draw the example drawable on top of the text.
        if (mExampleDrawable != null) {
            mExampleDrawable.setBounds(paddingLeft, paddingTop,
                    paddingLeft + contentWidth, paddingTop + contentHeight);
            mExampleDrawable.draw(canvas);
        }
    }

    /**
     * Gets the example string attribute value.
     *
     * @return The example string attribute value.
     */
    public String getExampleString() {
        return mExampleString;
    }

    /**
     * Sets the view"s example string attribute value. In the example view, this string
     * is the text to draw.
     *
     * @param exampleString The example string attribute value to use.
     */
    public void setExampleString(String exampleString) {
        mExampleString = exampleString;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example color attribute value.
     *
     * @return The example color attribute value.
     */
    public int getExampleColor() {
        return mExampleColor;
    }

    /**
     * Sets the view"s example color attribute value. In the example view, this color
     * is the font color.
     *
     * @param exampleColor The example color attribute value to use.
     */
    public void setExampleColor(int exampleColor) {
        mExampleColor = exampleColor;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example dimension attribute value.
     *
     * @return The example dimension attribute value.
     * @param dimension
     */
    public float getExampleDimension(float dimension) {
        return mExampleDimension;
    }

    /**
     * Sets the view"s example dimension attribute value. In the example view, this dimension
     * is the font size.
     *
     * @param exampleDimension The example dimension attribute value to use.
     */
    public void setExampleDimension(float exampleDimension) {
        mExampleDimension = exampleDimension;
        invalidateTextPaintAndMeasurements();
    }

    /**
     * Gets the example drawable attribute value.
     *
     * @return The example drawable attribute value.
     */
    public Drawable getExampleDrawable() {
        return mExampleDrawable;
    }

    /**
     * Sets the view"s example drawable attribute value. In the example view, this drawable is
     * drawn above the text.
     *
     * @param exampleDrawable The example drawable attribute value to use.
     */
    public void setExampleDrawable(Drawable exampleDrawable) {
        mExampleDrawable = exampleDrawable;
    }

    public void setMonth(int g) {
        this._month =  g;
    }

    public int getMonth() {
        return this._month;
    }
}