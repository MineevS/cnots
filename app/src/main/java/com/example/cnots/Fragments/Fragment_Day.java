package com.example.cnots.Fragments;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cnots.Activity.MainActivity;
import com.example.cnots.Adapters.Calendar_Adapter;
import com.example.cnots.Adapters.Nots_Adapter;
import com.example.cnots.Interfase.OnItemListener;
import com.example.cnots.R;
import com.example.cnots.SQLite.DBHelper;
import com.example.cnots.Views.View_Month;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Fragment_Day extends Fragment implements OnItemListener {

    private TextView text_Day;
    private RecyclerView RecyclerViewDayEvents;
    private LocalDate selectedDate;
    private ConstraintLayout _layoutInflater;
    private Activity activity;
    private int _step = 0;

    DBHelper dbHelper;
    Cursor cursor;
    SQLiteDatabase database;

    ArrayList<String> list = new ArrayList<>();

    public Fragment_Day(){
        selectedDate = LocalDate.now();
    }

    public Fragment_Day(int _i) {
        selectedDate = LocalDate.now();
        _step = _i;
        if(_i < 0)
        {
            selectedDate = selectedDate.minusDays(Math.abs(_i));
        }
        else if (_i > 0)
        {
            selectedDate = selectedDate.plusDays(Math.abs(_i));
        }
    }

    public Fragment_Day(LocalDate localdate, long _step) {
        selectedDate = localdate;
        this._step = (int) _step;
    }

    public Fragment_Day(int step, String[] id, LocalDate localDate) {
        this.selectedDate = localDate;
        this._step = (int) _step;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        activity = requireActivity();

        dbHelper = new DBHelper(this.activity);

        database = dbHelper.getWritableDatabase();
        cursor = database.query(DBHelper.TABLE_CONTACT, null, null, null, null, null, null);

        if(cursor.moveToFirst()){
            int idindex = cursor.getColumnIndex(DBHelper.KEY_ID);
            int titleindex = cursor.getColumnIndex(DBHelper.KEY_TITLE);
            int locationindex = cursor.getColumnIndex(DBHelper.KEY_LOCATION);
            int descriptionindex = cursor.getColumnIndex(DBHelper.KEY_DESCRIPTION);
            int timefromindex = cursor.getColumnIndex(DBHelper.KEY_TIMEFROM);
            int timebeforeindex = cursor.getColumnIndex(DBHelper.KEY_TIMEBEFORE);

            int post_index =-1;
            list.clear();

            do{

                String str_1 = cursor.getString(timefromindex);
                String str_2 = cursor.getString(timebeforeindex);
                String str_3 = selectedDate.getDayOfMonth() + "/" + selectedDate.getMonth().getValue() + "/" + selectedDate.getYear();
                String[] mas_1 = str_1.split("/");
                LocalDate timebefore;
                String[] mas_2 = str_2.split("/");

                int day = Integer.parseInt(mas_1[0]);
                int month = Integer.parseInt(mas_1[1]);
                int year = Integer.parseInt(mas_1[2]);

                LocalDate timefrom = LocalDate.of( year,month , day);

                if(!str_2.contains("/")){
                    timebefore =timefrom;
                }else{
                    timebefore =  LocalDate.of(Integer.parseInt(mas_2[2]), Integer.parseInt(mas_2[1]), Integer.parseInt(mas_2[0]));
                }

                if( (selectedDate.isEqual(timefrom)) || (selectedDate.isEqual(timebefore) || (selectedDate.isBefore(timebefore) && selectedDate.isAfter(timefrom))) ){

                        post_index = idindex;
                        list.add(cursor.getInt(idindex) + ":" + cursor.getString(timefromindex));

                }else{
                    //list.clear();
                    //list.add(cursor.getInt(idindex) + ":"+ "\n" +cursor.getString(titleindex) + " - "+ cursor.getString(timefromindex));
                }

            }while(cursor.moveToNext());
        }
        cursor.close();

        _layoutInflater = (ConstraintLayout) inflater.inflate(R.layout.layout_day, null);
        //_layoutInflater.invalidate();



        initwidget();
        //selectedDate = LocalDate.now() ;// minSDK:26;
        setMonthView();

        return _layoutInflater;
    }

    private void setMonthView() {

        String month = monthYearFromDate(selectedDate);
        String[] mon;
        mon = month.split(" ");

        if(mon[0].equals("December") && Integer.parseInt(mon[1]) != selectedDate.getYear())
        { // Баг в методе monthYearFromDate, который содержит стандарт преобразования,
            // который считает год с декабря по январь, а не с января по декабрь.

            text_Day.setText(mon[0] + " " + String.valueOf(Integer.parseInt(mon[1]) - 1));

        }else{
            text_Day.setText(monthYearFromDate(selectedDate));
        }
        /*

        ArrayList<String> daysInMonth = daysInMonthArray(selectedDate);
        if((LocalDate.now().getMonth() == selectedDate.getMonth()) && (LocalDate.now().getYear() == selectedDate.getYear())){
            Calendar_Adapter calendar__adapter = new Calendar_Adapter(daysInMonth, this, selectedDate);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getContext(), 7);//LinearLayoutManager.HORIZONTAL, false
            RecyclerViewDayEvents.setLayoutManager(layoutManager);
            RecyclerViewDayEvents.setAdapter(calendar__adapter);
        }else{
            Calendar_Adapter calendar__adapter = new Calendar_Adapter(daysInMonth, this);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getContext(), 7);//LinearLayoutManager.HORIZONTAL, false
            RecyclerViewDayEvents.setLayoutManager(layoutManager);
            RecyclerViewDayEvents.setAdapter(calendar__adapter);
        }
        */

        Nots_Adapter nots_adapter = new Nots_Adapter(list,  this, selectedDate);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getContext(), 1, LinearLayoutManager.VERTICAL, false);//LinearLayoutManager.HORIZONTAL, false
        RecyclerViewDayEvents.setLayoutManager(layoutManager);
        RecyclerViewDayEvents.setAdapter(nots_adapter);

    }

    private String monthYearFromDate(LocalDate Date) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM YYYY"); // minSDK:26;
        return Date.format(formatter);
    }

    private void initwidget() {

        this.text_Day = (TextView) _layoutInflater.getChildAt(0).findViewById(R.id.monthDayTV);

        this.RecyclerViewDayEvents = (RecyclerView) _layoutInflater.getChildAt(1);
    }

    public int get_step() {
        return this._step;
    }

    @Override
    public void onItemClick(int position, String Day_Text) {

        System.out.println(Day_Text);

        String[] id = Day_Text.split(":");

        ((MainActivity)getActivity()).InsertDay(id, selectedDate, _step);
    }
}
