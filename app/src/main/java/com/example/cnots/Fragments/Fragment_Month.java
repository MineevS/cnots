package com.example.cnots.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cnots.Activity.ListNoteActivity;
import com.example.cnots.Activity.MainActivity;
import com.example.cnots.Activity.SettingsActivity;
import com.example.cnots.Adapters.Calendar_Adapter;
import com.example.cnots.Interfase.OnItemListener;
import com.example.cnots.R;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Fragment_Month extends Fragment implements OnItemListener {

    ///////////////////////////////////////////////////////////////////////////////

    private TextView monthYearText;
    private RecyclerView calendarRecyclerView;
    private LocalDate selectedDate;
    private LinearLayout _layoutInflater;
    private Activity activity;
    private int _step = 0;

    public Fragment_Month(){
        selectedDate = LocalDate.now();
    }

    public Fragment_Month(int i) {
        selectedDate = LocalDate.now();
        _step = i;
        if(i < 0){

            //selectedDate = selectedDate.plusMonths(i);
            selectedDate = selectedDate.minusMonths(Math.abs(i));
        }
        else if (i > 0)
        {
            selectedDate = selectedDate.plusMonths(i);
        }
    }

    public Fragment_Month(LocalDate selectedDate, long step) {
        this.selectedDate = selectedDate;
        this._step = (int) step;

    }

    public int get_step() {
        return _step;
    }

    ///////////////////////////////////////////////////////////////

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){

        _layoutInflater = (LinearLayout) inflater.inflate(R.layout.layout_month, null);
        //_layoutInflater.invalidate();

        activity = requireActivity();

        initwidget();
        //selectedDate = LocalDate.now() ;// minSDK:26;
        setMonthView();

        return _layoutInflater;
    }


    String MontnFromENtoRUS(String mon){

        switch(mon){
            case("Feb"):
                return "Февраль";
            case("Mart"):
                return "Май";
            case("April"):
                return "Апрель";
            case("May"):
                return "Май";
            default:
                return mon;
        }
    }

    private void setMonthView()
    {
        String month = monthYearFromDate(selectedDate);
        String[] mon;
        mon = month.split(" ");

        if(mon[0].equals("December") && Integer.parseInt(mon[1]) != selectedDate.getYear())
        { // Баг в методе monthYearFromDate, который содержит стандарт преобразования,
            // который считает год с декабря по январь, а не с января по декабрь.

            monthYearText.setText(mon[0] + " " + String.valueOf(Integer.parseInt(mon[1]) - 1));

        }else{
            monthYearText.setText(monthYearFromDate(selectedDate));
        }

        ArrayList<String> daysInMonth = daysInMonthArray(selectedDate);
        if((LocalDate.now().getMonth() == selectedDate.getMonth()) && (LocalDate.now().getYear() == selectedDate.getYear())){
            Calendar_Adapter calendar__adapter = new Calendar_Adapter(daysInMonth, this, selectedDate);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getContext(), 7);//LinearLayoutManager.HORIZONTAL, false
            calendarRecyclerView.setLayoutManager(layoutManager);
            calendarRecyclerView.setAdapter(calendar__adapter);
        }else{
            Calendar_Adapter calendar__adapter = new Calendar_Adapter(daysInMonth, this);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getContext(), 7);//LinearLayoutManager.HORIZONTAL, false
            calendarRecyclerView.setLayoutManager(layoutManager);
            calendarRecyclerView.setAdapter(calendar__adapter);
        }
    }

    private ArrayList<String> daysInMonthArray(LocalDate Date) {
        ArrayList<String> daysInMonthArray = new ArrayList<>();
        YearMonth yearMonth = YearMonth.from(Date);

        int daysInMonth = yearMonth.lengthOfMonth();
        LocalDate firstOfMonth = selectedDate.withDayOfMonth(1);
        int dayOfWeek = firstOfMonth.getDayOfWeek().getValue();

        for(int i = 2; i <= 42; i++)
        {
            if(i <= dayOfWeek || i > daysInMonth + dayOfWeek)
            {
                daysInMonthArray.add("");
            }else
            {
                daysInMonthArray.add(String.valueOf(i - dayOfWeek));
            }
        }
        return daysInMonthArray;
    }

    private String monthYearFromDate(LocalDate Date) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM YYYY"); // minSDK:26;
        return Date.format(formatter);
    }

    private void initwidget()
    {
        this.calendarRecyclerView = (RecyclerView) _layoutInflater.getChildAt(2);
                //getLayoutInflater().inflate(R.id.calendarRecyclerView, null);
        //.findViewById(R.id.calendarRecyclerView);
        this.monthYearText = (TextView) _layoutInflater.getChildAt(0).findViewById(R.id.monthYearTV);
        //this.monthYearText = this.getView().findViewById(R.id.monthYearTV);
    }

    public void previousMonthAction(View view) {

        selectedDate = selectedDate.minusMonths(1);
        setMonthView();

    }

    public void nextMonthAction(View view) {
        selectedDate = selectedDate.plusMonths(1);
        setMonthView();
    }

    @Override
    public void onItemClick(int position, String Day_Text) {

        if(!Day_Text.equals("")){
            //Day_Text
            String  message = "Selected Date " + Day_Text + "  " + monthYearFromDate(selectedDate);
            //Toast.makeText(requireActivity(),message, Toast.LENGTH_SHORT).show();
            System.out.println(message);

            //this.
            ((MainActivity)getActivity()).InsertDay(LocalDate.of(selectedDate.getYear(),selectedDate.getMonth(),Integer.parseInt(Day_Text)));
            //Intent intent_1 = new Intent(activity, ListNoteActivity.class);
            //startActivity(intent_1);
            //Toast.makeText(activity,message, Toast.LENGTH_SHORT).show();
        }
    }

    public LocalDate get_localDate() {
        return this.selectedDate;
    }
}
