package com.example.cnots.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.cnots.Activity.ListNoteActivity;
import com.example.cnots.Activity.MainActivity;
import com.example.cnots.Interfase.OnItemListener;
import com.example.cnots.R;
import com.example.cnots.Views.View_Month;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Fragment_Year extends Fragment implements View.OnClickListener {

    private LocalDate selectedDate;
    private int _step = 0;
    private Activity activity;
    private TableLayout _layoutInflater;
    private TextView text_Year;
    private ArrayList<View_Month> Views_months = new ArrayList<View_Month>();

    public Fragment_Year(int _i) {
        selectedDate = LocalDate.now();
        _step = _i;
        if(_i < 0)
        {
            selectedDate = selectedDate.minusYears(Math.abs(_i));
        }
        else if (_i > 0)
        {
            selectedDate = selectedDate.plusYears(Math.abs(_i));
        }
    }

    ///////////////////////

    public int get_step() {
        return _step;
    }


    //////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

       _layoutInflater = (TableLayout) inflater.inflate(R.layout.layout_year, null);

        activity = requireActivity();
        initwidget();
        setYearView();

        return _layoutInflater;
    }

    private void setYearView() {
        this.text_Year.setText(LocaleDateYear(selectedDate));
        int i = 1;
        for (View_Month  view_month: Views_months) {

            //System.out.println(i);

            LocalDate start_selectedDate;

            if(i >=13){
                 start_selectedDate = LocalDate.of(selectedDate.getYear(),12,1);
            }else{

                start_selectedDate = LocalDate.of(selectedDate.getYear(),i,1);
            }


            //start_selectedDate.getDayOfWeek().getValue(); //

            int _st = (7 - (start_selectedDate.getDayOfWeek().getValue() - 1)) - 7;
            int _en = start_selectedDate.lengthOfMonth();

            view_month.set_start(_st);
            view_month.set_end(_en);

            view_month.setClickable(true);
            view_month.setOnClickListener(this);

            if((start_selectedDate.getMonth() == LocalDate.now().getMonth()) && (start_selectedDate.getYear() == LocalDate.now().getYear())){
                view_month.setSelectedDate(LocalDate.now());
            }

            view_month.invalidate();

            i +=1;
        }
        i = 0;
    }

    private String LocaleDateYear(LocalDate Date) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY"); // minSDK:26;

        return Date.format(formatter);
    }
    @Override
    public void onSaveInstanceState( Bundle outState ) {

    }

    private void initwidget()
    {
        this.text_Year = (TextView) _layoutInflater.getChildAt(0).findViewById(R.id.textYear_1);
        //TableRow tab_view = (TableRow) _layoutInflater.getChildAt(1);
        //RelativeLayout relativeLayout = (RelativeLayout) tab_view.getChildAt(0);
        //View_Month view_month = (View_Month) relativeLayout.getChildAt(2);
        //this.Views_months.add(view_month);
        if(this.Views_months.size() == 12){
            this.Views_months.clear();
        }

        int g = 1;
        for(int i = 1; i <= 4; i++)
        {
            for(int j = 0; j <= 2; j++)
            {
                // System.out.println(i + ": " + j);
                View_Month view_month = ((View_Month) ((RelativeLayout)((TableRow) _layoutInflater.getChildAt(i)).getChildAt(j)).getChildAt(2));
                view_month.setMonth(g);
                this.Views_months.add(view_month);
                g++;
            }
        }
    }

    @Override
    public void onClick(View v) {

            String  message = "Selected Date "  + "  " + selectedDate.getMonth();
            //Toast.makeText(requireActivity(),message, Toast.LENGTH_SHORT).show();
            System.out.println(message);
            if(_step == 0)
            {
                ((MainActivity)getActivity()).InsertMonth(LocalDate.of(selectedDate.getYear(),((View_Month)v).getMonth(), selectedDate.getDayOfMonth()));
            }else{
                ((MainActivity)getActivity()).InsertMonth(LocalDate.of(selectedDate.getYear(),((View_Month)v).getMonth(), _step));
            }
    }
}
