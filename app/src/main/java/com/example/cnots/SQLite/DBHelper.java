package com.example.cnots.SQLite;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

// Класс создания БД:
public class DBHelper extends SQLiteOpenHelper {

    public static final  int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "contactDb";
    public static final String TABLE_CONTACT = "contacts";

    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_LOCATION= "location";
    public static final String KEY_DESCRIPTION= "description";
    public static final String KEY_TIMEFROM= "time_from";
    public static final String KEY_TIMEBEFORE= "time_before";

    public DBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DBHelper(@Nullable Context context, @Nullable String name, int version) {
        super(context, name, null, version);
    }

    public DBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /*
        onCreate() — вызывается при первом создании базы данных
        onUpgrade() — вызывается при модификации базы данных
    Также используются другие методы класса:

        onDowngrade(SQLiteDatabase, int, int)  — вызывается в ситуации, обратной onUpgrade
        onOpen(SQLiteDatabase) - Вызывается при открытии БД.
        getReadableDatabase() - Возвращает БД для чтения.
        getWritableDatabase() - Возвращает БД для чтения и записи.
    * */

    public DBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE_CONTACT + "(" + KEY_ID +" "+ "integer primary key autoincrement, " + KEY_TITLE +" " + "text, " + KEY_LOCATION +" "+ "text, " + KEY_DESCRIPTION +" "+ "text, " + KEY_TIMEFROM +" " + "text, " + KEY_TIMEBEFORE +" "+ "text " + ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACT);

        onCreate(db);
    }
}
