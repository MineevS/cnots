package com.example.cnots;

import android.icu.util.LocaleData;

import com.example.cnots.Fragments.Fragment_Month;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void Variable_validation_step() {

        int x = 0;
        Fragment_Month fragment_month = new Fragment_Month(0);
        assertEquals(x, fragment_month.get_step());
    }

    @Test
    public void Variable_validation_selectedDate() {

        Fragment_Month fragment_month = new Fragment_Month(0);
        assertEquals(LocalDate.now(), fragment_month.get_localDate());
    }
}